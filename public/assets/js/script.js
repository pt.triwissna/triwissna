$(document).ready(function() {
    $(".nav-link").focus(function() {
        // Menambahkan kelas 'focused' ke elemen yang mendapatkan fokus
        $(this).addClass("focused");
    });

    $(".nav-link").blur(function() {
        // Menghapus kelas 'focused' dari elemen yang kehilangan fokus
        $(this).removeClass("focused");
    });
});
