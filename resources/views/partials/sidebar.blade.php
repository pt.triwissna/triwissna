<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand-demo">
        <a href="index.html" class="app-brand-link">
            <div class="app-brand-image-container">
                <img src="{{ asset('assets/img/logo/logo.png') }}" alt="Logo Sneat" class="app-brand-image">
            </div>
        </a>
        <span class="app-brand-text demo menu-text fw-bolder">PT. Triwisnna</span>
        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>    
    
    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py2">
        <!-- Dashboard -->
        <li class="menu-item active">
            <a href="index.html" class="menu-link">
                <object data="{{ asset('assets/img/icons/icon_svg/account_balance.svg') }}" type="image/svg+xml"></object>
                <div data-i18n="Analytics" style="margin-left: 10px;">Beranda</div>
            </a>
        </li>
        

        <li class="menu-header small text-uppercase">
            <span class="menu-header-text">Pages</span>
        </li>
        <li class="menu-item active">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <object data="{{ asset('assets/img/icons/icon_svg/orders.svg') }}" type="image/svg+xml"></object>
                <div data-i18n="Account Settings" class="menu_sidebar">Peminjaman</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item active">
                    <a href="#" class="menu-link">
                        <div data-i18n="Account">Peminjaman Aktif</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="pages-account-settings-notifications.html" class="menu-link">
                        <div data-i18n="Notifications">Riwayat Peminjaman</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <object data="{{ asset('assets/img/icons/icon_svg/orders.svg') }}" type="image/svg+xml"></object>
                <div data-i18n="Authentications" class="menu_sidebar">Perbaikan</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item">
                    <a href="auth-login-basic.html" class="menu-link" target="_blank">
                        <div data-i18n="Basic">Perbaikan Aktif</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="auth-register-basic.html" class="menu-link" target="_blank">
                        <div data-i18n="Basic">Riwayat Perbaikan</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="menu-item">
            <a href="https://github.com/themeselection/sneat-html-admin-template-free/issues"
                target="_blank" class="menu-link">
                <object data="{{ asset('assets/img/icons/icon_svg/orders.svg') }}" type="image/svg+xml"></object>
                <div data-i18n="Support" class="menu_sidebar">Data Unit</div>
            </a>
        </li>
    </ul>
</aside>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        const menuItems = document.querySelectorAll('.menu-item');

        menuItems.forEach(function (menuItem) {
            const svgIcon = menuItem.querySelector('object');
            const svgPath = svgIcon.contentDocument.querySelector('path');

            menuItem.addEventListener('click', function () {
                // Hapus kelas "active" dari semua elemen menu
                menuItems.forEach(function (item) {
                    item.classList.remove('active');
                });

                // Tambahkan kelas "active" ke elemen saat ini
                menuItem.classList.add('active');

                // Atur warna fill SVG berdasarkan kelas "active" atau "inactive"
                if (menuItem.classList.contains('active')) {
                    svgPath.setAttribute('fill', '#525252');
                } else {
                    svgPath.setAttribute('fill', '#B0B0B0');
                }
            });
        });
    });
</script>

